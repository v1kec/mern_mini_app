// Declare all the packages that we will use
const express = require('express');
const mongoose = require('mongoose');
const items = require('./routes/api/items');
const users = require('./routes/api/users');
const auth = require('./routes/api/auth');
const config = require('config');


// Declare app const that will run express;
const app = express();

// Express Middleware
app.use(express.json());

// DB config
const db = config.get('mongoURI');

mongoose
    .connect(db, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true})
    .then(() => {
        console.log('Database connected...')
    })
    .catch(err => console.log(err));

// Use routes
app.use('/api/items', items)
app.use('/api/users', users)
app.use('/api/auth', auth)

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`))
