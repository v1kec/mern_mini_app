import React from 'react'
import {logout} from '../../actions/authAction'
import {connect} from 'react-redux';
import { NavLink } from 'reactstrap';
import PropTypes from 'prop-types'

const Logout = (props) => {
    const propTypes = {
        logout: PropTypes.func.isRequired
    }

    return (
        <div>
            <NavLink onClick={props.logout} href="#">
                Logout
            </NavLink>
        </div>
    )
}

export default connect(
    null,
    {logout}
)(Logout);
