import React, {useState, useEffect, useRef} from 'react';
import {Button,Modal,ModalHeader,ModalBody,Form,FormGroup,Label,Input, NavLink, Alert} from 'reactstrap';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'
import {register} from '../../actions/authAction'

const RegisterModal = (props) => {

    const [registerModal, setRegisterModal] = useState(
        {
            modal: false, 
            name: '', 
            email: '', 
            password: '', 
            msg: null
        }
    )
    const propTypes = {
        isAuthenticated: PropTypes.bool,
        error: PropTypes.object.isRequired
    }
    const error = props.error
    const isAuth = props.isAuthenticated
    // Get prevState
    const usePrevious = (value) => {
        const ref = useRef();
        useEffect(() => {
          ref.current = value;
        }, [value]);
        return ref.current;
    }
        
    // // 
    const prevProps = usePrevious(error);
    
    useEffect(() => {
        const {error, isAuthenticated} = props
        if(error !== prevProps) {
            // Check for register error
            if(error.id === "REGISTER_FAIL"){
                setRegisterModal({...registerModal, msg: error.msg.msg})
            } else {
                setRegisterModal({
                    modal: false, 
                    name: '', 
                    email: '', 
                    password: '', 
                    msg: null
                })
            }
        }
        // If authenticated, close modal
        if(registerModal.modal) {
            if(isAuthenticated) {
                toggle();
            }
        }

    }, [error,isAuth])

    const toggle = () => {
        setRegisterModal({modal: !registerModal.modal})
    }

    const onChange = (e) => {
        setRegisterModal({...registerModal, [e.target.name]: e.target.value})
    }

    const onSubmit = e => {
        e.preventDefault();
        const {name, email, password} = registerModal

        // Create new user
        const newUser = {
            name,
            email,
            password
        }
        // Attempt to register
        props.register(newUser)

        // toggle();
    }

    return (
        <div>
            <NavLink onClick={toggle} href="#">
                Register
            </NavLink>

            <Modal
                isOpen={registerModal.modal}
                toggle={toggle}
            >
                <ModalHeader toggle={toggle}>Register</ModalHeader>
                <ModalBody>
                    {registerModal.msg ? <Alert color="danger">{registerModal.msg}</Alert> : null}
                    <Form onSubmit={e => onSubmit(e)}>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input 
                                type="text"
                                name="name"
                                id="name"
                                placeholder="Name"
                                className="mb-3"
                                onChange={e => onChange(e)}
                            />
                            <Label for="email">Email</Label>
                            <Input 
                                type="email"
                                name="email"
                                id="email"
                                placeholder="Email"
                                className="mb-3"
                                onChange={e => onChange(e)}
                            />
                            <Label for="password">Password</Label>
                            <Input 
                                type="password"
                                name="password"
                                id="password"
                                placeholder="Password"
                                className="mb-3"
                                onChange={e => onChange(e)}
                            />
                            <Button
                                color="dark"
                                style={{marginTop: '2em'}}
                                block
                            >
                                Register
                            </Button>
                        </FormGroup>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    )
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    error: state.error,
    register: PropTypes.func.isRequired
})

export default connect(mapStateToProps, {register})(RegisterModal)
