import React, {useState} from 'react';
import {Button,Modal,ModalHeader,ModalBody,Form,FormGroup,Label,Input} from 'reactstrap';
import {connect} from 'react-redux';
import {addItem} from '../actions/itemActions'
import PropTypes from 'prop-types'

const ItemModal = (props) => {

    const [itemModal, setItemModal] = useState({modal: false, name: ''})

    const propTypes = {
        isAuthenticated: PropTypes.bool
    }

    const toggle = () => {
        setItemModal({modal: !itemModal.modal})
    }

    const onChange = (e) => {
        setItemModal({...itemModal, [e.target.name]: e.target.value})
    }

    const onSubmit = e => {
        e.preventDefault();

        const newItem = {
            name: itemModal.name
        }

        props.addItem(newItem);
        toggle();
    }

    return (
        <div>
            {props.isAuthenticated ? <Button 
                color="dark"
                style={{marginBottom: '2rem'}}
                onClick={toggle}
            >Add item
            </Button> : <h4 className="mb-3 ml-4">Please log in to manage items</h4>}

            

            <Modal
                isOpen={itemModal.modal}
                toggle={toggle}
            >
                <ModalHeader toggle={toggle}>Add to Shopping List</ModalHeader>
                <ModalBody>
                    <Form onSubmit={e => onSubmit(e)}>
                        <FormGroup>
                            <Label for="item">Item</Label>
                            <Input 
                                type="text"
                                name="name"
                                id="item"
                                placeholder="Add shopping item"
                                onChange={e => onChange(e)}
                            />
                            <Button
                                type="submit"
                                color="dark"
                                style={{marginTop: '2em'}}
                                block
                            >
                                Add Item
                            </Button>
                        </FormGroup>
                    </Form>
                </ModalBody>
            </Modal>
        </div>
    )
}

const mapStateToProps = state => ({
    item: state.item,
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, {addItem})(ItemModal)
