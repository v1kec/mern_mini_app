import {GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING} from './types'
import axios from 'axios'
import {tokenConfig} from './authAction'
import {returnErrors} from './errorActions'

export const getItems = () => async dispatch => {
    dispatch(setItemsLoading());
    await fetch('/api/items',{
        method: "GET"
    })
        .then(res => res.json())
        .then(res => dispatch({
            type: GET_ITEMS,
            payload: res
        }))
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)))
}

export const deleteItem = id => (dispatch, getState) => {

    axios
        .delete(`/api/items/${id}`, tokenConfig(getState))
        .then(res =>
            dispatch({
                type: DELETE_ITEM,
                payload: id
            })
        )
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)))

    // await fetch(`/api/items/${id}`, {
    //     method: 'DELETE',
    //     headers: {
    //         'Content-Type': 'application/json'
    //     },
    //     body: tokenConfig(getState)
    // })
    //     .then(res => res.json())
    //     .then(res => dispatch({
    //         type: DELETE_ITEM,
    //         payload: id
    //     }))
    //     .catch(err => dispatch(returnErrors(err.response.data, err.response.status)))
}

export const addItem = item => async (dispatch, getState) => {
    axios.post('/api/items', item, tokenConfig(getState))
        .then(res => 
            dispatch({
                type: ADD_ITEM,
                payload: res.data
            }))
            .catch(err => dispatch(returnErrors(err.response.data, err.response.status)))
    // await fetch('/api/items', {
    //     method: 'POST',
    //     body: item
    // })
    //     .then(res => res.json())
    //     .then(data => dispatch({
    //         type: ADD_ITEM,
    //         payload: data
    //     }))
    //     .catch(err => console.log(err))

}

export const setItemsLoading = () => {
    return {
        type: ITEMS_LOADING
    }
}